package ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import data.GameAPI
import data.GameShort
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

class LobbyViewModel(application: Application) : AndroidViewModel(application) {

    private val compositeDisposable = CompositeDisposable()

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }

    val allGames: MutableLiveData<List<GameShortItem>> = MutableLiveData()
    val navigateToVoteGame: MutableLiveData<String> = MutableLiveData()
    val navigateToStepGame: MutableLiveData<String> = MutableLiveData()
    val isLoading: MutableLiveData<Boolean> = MutableLiveData()

    private val limit: Int = 10
    private var offset: Int = 0
    private var count: Int = 0
    private var games: MutableList<GameShort> = mutableListOf()
    private var adapterItems: MutableList<GameShortItem> = mutableListOf()

    private fun getActiveGames(
        games: List<GameShort>,
        offset: Int,
    ): Pair<List<GameShortItem>, Int> {
        val result: MutableList<GameShortItem> = mutableListOf()
        val currentTime = System.currentTimeMillis()
        var index = 0
        var newOffset = offset
        while (index < games.size) {
            var currentGame = games[index]
            if (currentGame.endTime > currentTime / 1000L) {
                result.add(convert(currentGame))
            } else {
                newOffset--
            }
            index++
        }
        return result to newOffset
    }

    fun game(gameApi: GameAPI?) {
        gameApi?.let { api ->
            offset = 0
            games.clear()
            adapterItems.clear()
            val request = getGames(api, offset)
            compositeDisposable.add(request)
        }
    }

    fun settings(position: Int) {
        val game = allGames.value?.get(position)
        if (game?.type == 0) {
            navigateToVoteGame.value = game.id
        }
        if (game?.type == 1) {
            navigateToStepGame.value = game.id
        }
    }

    fun onLoadMore(gameApi: GameAPI) {
        if (isLoading.value == true || offset >= count) {
            return
        }
        offset += limit
        getGames(gameApi, offset)
    }

    private fun getGames(gameApi: GameAPI, offset: Int) =
        gameApi.getGames(limit, offset)
            .map {
                val adapterItems = it.games?.map { game -> convert(game) }
                    ?: Collections.emptyList()
                ViewState(
                    games = it.games ?: Collections.emptyList(),
                    adapterItems = adapterItems,
                    count = it.count
                )
            }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { isLoading.value = true }
            .subscribe(
                this::onSuccess,
                this::onError
            )

    private fun convert(game: GameShort): GameShortItem =
        GameShortItem(
            game.id,
            game.title,
            game.endTime - System.currentTimeMillis() / 1000L,
            game.score,
            game.type
        )

    private fun onSuccess(viewState: ViewState) {
        val newGames = mutableListOf<GameShort>()
        newGames.addAll(games)
        newGames.addAll(viewState.games)
        games = newGames
        val newAdapterItems = mutableListOf<GameShortItem>()
        newAdapterItems.addAll(adapterItems)
        newAdapterItems.addAll(viewState.adapterItems)
        adapterItems = newAdapterItems
        count = viewState.count
        isLoading.value = false
    }

    private fun onError(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
        isLoading.value = false
    }

    fun onActivityCreated() {
        val run = Observable.interval(1000L, TimeUnit.MILLISECONDS)
            .map { getActiveGames(games, offset) }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .subscribe(
                { (newGames, newOffset) -> onNextRun(newGames, newOffset) },
                this::onErrorRun
            )
        compositeDisposable.add(run)
    }

    private fun onNextRun(newGames: List<GameShortItem>, newOffset: Int) {
        offset = newOffset
        adapterItems = newGames.toMutableList()
        allGames.value = newGames
    }

    private fun onErrorRun(unit: Throwable) {
        Log.e(TAG, "null")
    }

    private class ViewState(
        val games: List<GameShort>,
        val adapterItems: List<GameShortItem>,
        val count: Int,
    )
}
