package data

import com.google.gson.annotations.SerializedName

data class GameSettings(
    @SerializedName("duration_max")
    val durationMax: Int,
    @SerializedName("duration_recommended")
    val durationRecommended: Int,
    @SerializedName("selected_game_type")
    val selectedGameType: Int,
    @SerializedName("available_game_types")
    val availableGameTypes: List<Int>
)