package ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.legion.R
import java.util.concurrent.Executors

class LobbyAdapter(
    diffItemCallback : DiffUtil.ItemCallback<GameShortItem>,
    private val viewOnClickListener: (Int) -> Unit
) : ListAdapter<GameShortItem, LobbyAdapter.ItemViewHolder>(
    AsyncDifferConfig.Builder(diffItemCallback)
        .setBackgroundThreadExecutor(Executors.newSingleThreadExecutor())
        .build()
) {

    class ItemViewHolder(view: View, viewOnClickListener: (Int) -> Unit) : RecyclerView.ViewHolder(
        view) {
        private val name: TextView = view.findViewById<View>(R.id.name) as TextView
        private val duration: TextView = view.findViewById<View>(R.id.duration) as TextView
        private val yes: TextView = view.findViewById<View>(R.id.yes) as TextView
        private val no: TextView = view.findViewById<View>(R.id.no) as TextView
        private val iconType: ImageView = view.findViewById(R.id.game_type_icon)
        private val start = itemView.findViewById<ConstraintLayout>(R.id.item)
        init {
            start.setOnClickListener { viewOnClickListener(absoluteAdapterPosition) }
        }

        fun bind(item: GameShortItem) {
            duration.text = item.lastTime.toString()
            name.text = item.title
            yes.text = item.score[0].toString()
            no.text = item.score[1].toString()
            if (item.type == 0){
                iconType.setImageResource(R.drawable.ic_vote)
            } else {
                iconType.setImageResource(R.drawable.ic_steps)
            }
        }
    }

    fun addData(response: List<GameShortItem>) {
        submitList(response)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_vote, parent, false)
        return ItemViewHolder(view, viewOnClickListener)
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

