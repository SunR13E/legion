package ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProviders
import com.example.legion.R
import kotlinx.android.synthetic.main.item_vote.*

class VoteGameActivity : AppCompatActivity() {

    lateinit var viewModel : VoteGameViewModel
    lateinit var id: String

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_vote_game)

        id = intent.extras?.getString("SETTINGS").orEmpty()
        val time = findViewById<TextView>(R.id.timers)
        val count1 = findViewById<TextView>(R.id.count_player1)
        val count2 = findViewById<TextView>(R.id.count_player2)
        val button1 = findViewById<TextView>(R.id.button_player1)
        val button2 = findViewById<TextView>(R.id.button_player2)
        val sessionTitle = findViewById<TextView>(R.id.title_session)
        viewModel = ViewModelProviders.of(this).get(VoteGameViewModel::class.java)
        viewModel.id = id

        viewModel.onActivityCreated((this.application as? GameApp)?.gameApi)

        viewModel.data.observe(this, {
            if(it == null)
                return@observe
            count1.text = it[0].toString()
            count2.text = it[1].toString()

        })

        viewModel.titleData.observe(this, {sessionTitle.text = it})

        viewModel.time.observe(this, {
            time.text = it.toString()
        })

        viewModel.updateScores.observe(this, { viewModel.game((application as? GameApp)?.gameApi)} )

        viewModel.close.observe(this, {
            button1.isEnabled = false
            button2.isEnabled = false
            gameEnd(leftScore = it[0], rightScore = it[1])
        })

        button1.setOnClickListener {
            viewModel.increase1()
            button2.visibility = View.GONE
        }

        button2.setOnClickListener {
            viewModel.increase2()
            button1.visibility = View.GONE
        }
    }


    private fun gameEnd(leftScore : Int, rightScore : Int){
        val intent = Intent(this, VoteGameEndActivity::class.java)
        intent.putExtra("LEFT RESULT", leftScore.toString())
        intent.putExtra("RIGHT RESULT", rightScore.toString())
        startActivity(intent)
        finish()
        viewModel.ActivityOnDestroy()
    }
}