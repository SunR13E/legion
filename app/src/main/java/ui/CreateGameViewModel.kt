package ui

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import data.CreateGameRequest
import data.GameAPI
import data.GameSettings
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

private const val GAME_TYPE_VOTE = 0
private const val GAME_TYPE_STEP = 1
private const val GAME_DURATION_MAX = 300
private const val GAME_DURATION_MIN = 35

class CreateGameViewModel(application: Application) : AndroidViewModel(application) {
    private val compositeDisposable = CompositeDisposable()

    val startGame: MutableLiveData<GameNavigation> = MutableLiveData()
    val isStepGameType: MutableLiveData<Boolean> = MutableLiveData()
    val durationRecommended: MutableLiveData<Int> = MutableLiveData()
    val durationMax: MutableLiveData<Int> = MutableLiveData()
    val focusOnDuration: MutableLiveData<Unit> = MutableLiveData()
    val focusOnTitle: MutableLiveData<Unit> = MutableLiveData()
    val isErrorDurationMax: MutableLiveData<Pair<Boolean, Int>> = MutableLiveData()
    val isErrorDurationMin: MutableLiveData<Pair<Boolean, Int>> = MutableLiveData()
    val isErrorTitle: MutableLiveData<Boolean> = MutableLiveData()

    private var gameType: Int = 0

    fun onCreate(gameApi: GameAPI?) {
        gameApi?.let { api ->
            val request = api.getGameSettings()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    ::onSuccess,
                    ::onError
                )
            compositeDisposable.add(request)
        }
    }

    fun onSelectType(isChecked: Boolean) {
        isStepGameType.value = isChecked
        gameType = if (!isChecked) {
            0
        } else {
            1
        }
    }

    fun setGame(gameApi: GameAPI?, title: String, duration: String) {
        //Превалидация параметров
        var isNotValid = false
        if (title.isBlank()) {
            isErrorTitle.value = true
            focusOnTitle.value = Unit
            isNotValid = true
        }

        if (duration.isBlank()) {
            if (!isNotValid) {
                focusOnDuration.value = Unit
            }
            isErrorDurationMin.value = true to GAME_DURATION_MIN
            isNotValid = true
        }

        if (isNotValid) {
            return
        }

        val durationRequest = duration.toInt()
        val isDurationMinError = durationRequest < GAME_DURATION_MIN
        val isDurationMaxError = durationRequest > durationMax.value ?: GAME_DURATION_MAX
        if (isDurationMinError) {
            isErrorDurationMin.value = isDurationMinError to GAME_DURATION_MIN
            return
        }
        if (isDurationMaxError) {
            isErrorDurationMax.value =
                isDurationMaxError to (durationMax.value ?: GAME_DURATION_MAX)
            return
        }

        gameApi?.let { api ->
            val game = Single.just(title to durationRequest)
                .map { (title, duration) ->
                    CreateGameRequest(
                        title = title,
                        duration = duration,
                        type = gameType
                    )
                }
                .flatMap { settings ->
                    api.createGame(settings).map { game -> game.id to settings.type }
                }
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { (gameId, gameType) -> onSuccess(gameId, gameType) },
                    this::onError,
                )
            compositeDisposable.add(game)
        }
    }

    private fun onSuccess(gameId: String, gameType: Int) {
        if (gameType == GAME_TYPE_VOTE) {
            startGame.value = GameNavigation.Vote(gameId)
        }
        if (gameType == GAME_TYPE_STEP) {
            startGame.value = GameNavigation.Step(gameId)
        }
    }

    private fun onSuccess(settings: GameSettings) {
        durationMax.value = settings.durationMax
        durationRecommended.value = settings.durationRecommended
        isStepGameType.value = settings.selectedGameType == GAME_TYPE_STEP
    }

    private fun onError(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    override fun onCleared() {
        compositeDisposable.dispose()
        super.onCleared()
    }
}
