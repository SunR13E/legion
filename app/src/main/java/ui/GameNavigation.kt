package ui

sealed class GameNavigation(val gameId: String) {
    class Vote(gameId: String) : GameNavigation(gameId)
    class Step(gameId: String) : GameNavigation(gameId)
    class Result(title: String, leftScores: Int, rightScores: Int)
}

