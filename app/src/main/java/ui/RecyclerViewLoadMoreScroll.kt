package ui

import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class RecyclerViewLoadMoreScroll(private val layoutManager: LinearLayoutManager) :
    RecyclerView.OnScrollListener() {

    private var visibleThreshold = 5
    private lateinit var mOnLoadMoreListener: OnLoadMoreListener
    private var lastVisibleItem: Int = 0
    private var totalItemCount:Int = 0


    fun setOnLoadMoreListener(mOnLoadMoreListener: OnLoadMoreListener) {
        this.mOnLoadMoreListener = mOnLoadMoreListener
    }

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        if (dy <= 0) return

        totalItemCount = layoutManager.itemCount
        lastVisibleItem = layoutManager.findLastVisibleItemPosition()

        if(lastVisibleItem == RecyclerView.NO_POSITION) {
            return
        }

        if(lastVisibleItem + visibleThreshold <= totalItemCount) {
            return
        }

        if(lastVisibleItem + visibleThreshold > totalItemCount) {
            mOnLoadMoreListener.onLoadMore()
        }
    }
}
