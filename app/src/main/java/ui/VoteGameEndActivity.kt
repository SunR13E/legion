package ui

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.legion.R

class VoteGameEndActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_game_end)

        val leftScore = intent.extras?.getString("LEFT RESULT").orEmpty()
        val rightScore = intent.extras?.getString("RIGHT RESULT").orEmpty()
        val left = findViewById<TextView>(R.id.leftScore)
        val right = findViewById<TextView>(R.id.rightScore)
        val finalScore = findViewById<TextView>(R.id.finalScore)
        val finalScoreText = findViewById<TextView>(R.id.finalScoreText)
        val close = findViewById<TextView>(R.id.close)

        left.text = leftScore
        right.text = rightScore

        if(leftScore.toInt() > rightScore.toInt()){
            finalScore.text = leftScore
            finalScoreText.setText(R.string.yes)
        }
        if(leftScore.toInt() < rightScore.toInt()){
            finalScore.text = rightScore
            finalScoreText.setText(R.string.no)
        }

        close.setOnClickListener {
            finish()
        }
    }

}