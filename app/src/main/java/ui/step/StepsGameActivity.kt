package ui.step

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.animation.LinearInterpolator
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.view.isGone
import androidx.core.view.isInvisible
import androidx.core.view.isVisible
import androidx.lifecycle.ViewModelProviders
import com.example.legion.R
import data.GameAPI
import kotlinx.android.synthetic.main.activity_steps_game.*
import ui.GameApp
import ui.VoteGameEndActivity

class StepsGameActivity : AppCompatActivity() {

    lateinit var viewModel: StepsGameViewModel
    private val gameApi: GameAPI by lazy { (application as GameApp).gameApi }

    private lateinit var leftButton: TextView
    private lateinit var rightButton: TextView
    private lateinit var duration: TextView
    private lateinit var step: TextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_steps_game)

        leftButton = findViewById(R.id.button1)
        rightButton = findViewById(R.id.button2)
        duration = findViewById(R.id.duration)
        step = findViewById(R.id.click)
        val progressBar = findViewById<ProgressBar>(R.id.progress)
        val waitingEnd = findViewById<TextView>(R.id.waiting)

        val gameId = intent.extras?.getString("SETTINGS").orEmpty()
        viewModel = ViewModelProviders.of(this).get(StepsGameViewModel::class.java)
        viewModel.onCreate(gameApi, gameId)

        setSelectSide()
        viewModel.isGameStarted.observe(this, {
            if(it){
                setStartGame()
            }
        })

        viewModel.time.observe(this, {
            if (duration.isInvisible || duration.isGone) {
                duration.isVisible = true
            }
            duration.text = it.toString()
        })

        viewModel.getClickedPoint.observe(this, {
            if (step.isInvisible || step.isGone) {
                step.isVisible = true
            }
            if (it.isPositive) {
                step.setTextColor(ContextCompat.getColor(this, R.color.colorSecondary))
            } else {
                step.setTextColor(ContextCompat.getColor(this, R.color.colorError))
            }
            step.animate()
                .setInterpolator(LinearInterpolator())
                .setDuration(200)
                .scaleX(4F)
                .scaleY(4F)
                .alpha(1F)
                .withStartAction { step.text = it.value }
                .withEndAction {
                    step.animate()
                        .setInterpolator(LinearInterpolator())
                        .setDuration(200)
                        .scaleX(1F)
                        .scaleY(1F)
                        .alpha(0F)
                        .setStartDelay(100)
                        .withEndAction { step.text = "" }
                        .start()
                }.start()
        })

        viewModel.waiting.observe(this, {
            leftButton.isEnabled = false
            rightButton.isEnabled = false
            leftButton.visibility = View.INVISIBLE
            rightButton.visibility = View.INVISIBLE
            step.visibility = View.GONE
            waitingEnd.visibility = View.VISIBLE
            waitingEnd.setText(R.string.waiting)
            progressBar.visibility = View.VISIBLE
        })

        viewModel.isClicksEnabled.observe(this, {
            leftButton.isEnabled = it
            rightButton.isEnabled = it
        })
        viewModel.isLeftButtonActivated.observe(this, {
            if (it) {
                leftButton.animate()
                    .setDuration(100)
                    .alpha(1F)
                    .start()
            } else {
                leftButton.animate()
                    .setDuration(100)
                    .alpha(0F)
                    .start()
            }
        })
        viewModel.isRightButtonActivated.observe(this, {
            if (it) {
                rightButton.animate()
                    .setDuration(100)
                    .alpha(1F)
                    .start()

            } else {
                rightButton.animate()
                    .setDuration(100)
                    .alpha(0F)
                    .start()
            }
        })

        leftButton.setOnClickListener {
            viewModel.onClick(gameApi, 0)
        }

        rightButton.setOnClickListener {
            viewModel.onClick(gameApi, 1)
        }

        viewModel.closeWithResult.observe(this, { scores ->
            waitingEnd.visibility = View.VISIBLE
            waitingEnd.setText(R.string.waiting)
            progressBar.visibility = View.VISIBLE
            gameEnd(scores[0], scores[1])
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.onDestroy()
    }

    private fun gameEnd(leftScore: Int, rightScore: Int) {
        val intent = Intent(this, VoteGameEndActivity::class.java)
        intent.putExtra("LEFT RESULT", leftScore.toString())
        intent.putExtra("RIGHT RESULT", rightScore.toString())
        startActivity(intent)
        finish()
    }

    private fun setSelectSide() {
        step.setText(R.string.choose_side)
        step.setTextColor(ContextCompat.getColor(this, R.color.colorSecondary))
        step.alpha = 1F
        leftButton.setTextColor(ContextCompat.getColor(this, R.color.colorSecondary))
        leftButton.setText(R.string.yes)
        leftButton.alpha = 1F
        rightButton.setTextColor(ContextCompat.getColor(this, R.color.colorError))
        rightButton.setText(R.string.no)
        rightButton.alpha = 1F
    }

    private fun setStartGame() {
        step.text = ""
        step.alpha = 0F
        leftButton.setTextColor(ContextCompat.getColor(this, R.color.colorSecondary))
        leftButton.setText(R.string.click)
        leftButton.alpha = 0F
        rightButton.setTextColor(ContextCompat.getColor(this, R.color.colorSecondary))
        rightButton.setText(R.string.click)
        rightButton.alpha = 0F
    }
}
