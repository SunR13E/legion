package ui

data class GameShortItem(
    val id: String,
    val title: String,
    val lastTime: Long,
    val score: List<Int>,
    val type : Int
)