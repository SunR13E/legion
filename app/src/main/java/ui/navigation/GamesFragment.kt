package ui.navigation

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.example.legion.R
import com.google.android.material.floatingactionbutton.FloatingActionButton
import data.GameAPI
import kotlinx.android.synthetic.main.fragment_home.*
import ui.*
import ui.step.StepsGameActivity

class GamesFragment() : Fragment() {

    private lateinit var lobbyViewModel: LobbyViewModel
    lateinit var adapter: LobbyAdapter
    lateinit var scrollListener: RecyclerViewLoadMoreScroll
    lateinit var mainHandler: Handler

    private val gameApi: GameAPI by lazy { (activity?.application as GameApp).gameApi }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lobbyViewModel = ViewModelProviders.of(this).get(LobbyViewModel::class.java)
        mainHandler = Handler(Looper.getMainLooper())
        lobbyViewModel.onActivityCreated()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val createGame = view.findViewById<FloatingActionButton>(R.id.freePlay)
        createGame.setOnClickListener {
            val intent = Intent(activity, CreateGameActivity::class.java)
            startActivity(intent)
        }

        val linearLayoutManager = LinearLayoutManager(activity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL
        scrollListener = RecyclerViewLoadMoreScroll(linearLayoutManager)
        scrollListener.setOnLoadMoreListener(object : OnLoadMoreListener {
            override fun onLoadMore() {
                lobbyViewModel.onLoadMore(gameApi)
            }
        })

        lobbyList.addOnScrollListener(scrollListener)
        lobbyList.layoutManager = linearLayoutManager

        val diffCallback = object : DiffUtil.ItemCallback<GameShortItem>() {
            override fun areItemsTheSame(oldItem: GameShortItem, newItem: GameShortItem): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(
                oldItem: GameShortItem,
                newItem: GameShortItem,
            ): Boolean =
                oldItem == newItem

            override fun getChangePayload(oldItem: GameShortItem, newItem: GameShortItem): Any? {
                return newItem
            }
        }

        adapter = LobbyAdapter(diffCallback) { lobbyViewModel.settings(it) }
        adapter.notifyDataSetChanged()
        lobbyList.adapter = adapter
        lobbyViewModel.allGames.observe(viewLifecycleOwner, { adapter.addData(it) })
        lobbyViewModel.navigateToStepGame.observe(viewLifecycleOwner, { startSteps(it) })
        lobbyViewModel.navigateToVoteGame.observe(viewLifecycleOwner, { startVote(it) })

        val refresh = view.findViewById<SwipeRefreshLayout>(R.id.refresh)
        lobbyViewModel.isLoading.observe(viewLifecycleOwner, { refresh.isRefreshing = it })
        refresh.setOnRefreshListener {
            lobbyViewModel.game((activity?.application as? GameApp)?.gameApi)
        }
    }

    override fun onResume() {
        super.onResume()
        lobbyViewModel.game((activity?.application as? GameApp)?.gameApi)
    }

    private fun startSteps(id: String) {
        val intent = Intent(activity, StepsGameActivity::class.java)
        intent.putExtra("SETTINGS", id)
        startActivity(intent)
    }

    private fun startVote(id: String) {
        val intent = Intent(activity, VoteGameActivity::class.java)
        intent.putExtra("SETTINGS", id)
        startActivity(intent)
    }

}