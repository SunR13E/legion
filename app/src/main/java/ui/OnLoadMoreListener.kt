package ui

interface OnLoadMoreListener {
    fun onLoadMore()
}