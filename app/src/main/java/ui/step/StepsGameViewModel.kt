package ui.step

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import data.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.*
import java.util.concurrent.TimeUnit

private const val AMOUNT_CLICKED_STEPS_TO_SEND = 10

class StepsGameViewModel : ViewModel() {

    val time: MutableLiveData<Int> = MutableLiveData()
    val closeWithResult: MutableLiveData<List<Int>> = MutableLiveData()
    val titleData: MutableLiveData<String> = MutableLiveData()
    val isGameStarted: MutableLiveData<Boolean> = MutableLiveData()
    val isLeftButtonActivated: MutableLiveData<Boolean> = MutableLiveData()
    val isRightButtonActivated: MutableLiveData<Boolean> = MutableLiveData()
    val isClicksEnabled: MutableLiveData<Boolean> = MutableLiveData()
    val waiting: MutableLiveData<Unit> = MutableLiveData()
    val getClickedPoint: MutableLiveData<StepGamePoint> = MutableLiveData()

    private val compositeDisposable = CompositeDisposable()
    private var id: String = ""
    private var gameEnd: Long = 0
    private var lastDuration: Int = 0
    private var title: String = ""

    private val timerUpdateInterval = Observable.interval(1000L, TimeUnit.MILLISECONDS)
    private var timerSkip: Disposable? = null
    private var timerToStartNextStep: Disposable? = null

    private var gameSide = 0
    private var stepIndex = 0
    private var steps: List<Steps> = Collections.emptyList()
    private val clickedSteps: MutableList<GamePoint> = mutableListOf()
    private val clickedStepsSendQueue: MutableList<SendStepRequest> = mutableListOf()
    private var isSideSelected: Boolean = false

    fun onCreate(gameApi: GameAPI?, gameId: String) {
        id = gameId
        gameApi?.let { api ->
            val getGameRequest = api.getStepsGame(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { onSuccessGame(api, it) },
                    this::onErrorTime
                )
            compositeDisposable.add(getGameRequest)
        }
    }

    fun onSelectSide(selectedSide: Int) {
        if (!isSideSelected) {
            isSideSelected = true
            isGameStarted.value = true
        }
        gameSide = selectedSide
        startNextStepTimer(500)
    }

    private fun sendStepsFromQueue(
        gameApi: GameAPI,
        gameId: String,
        queue: List<SendStepRequest>,
    ): Single<List<SendStepRequest>> {
        return Observable.fromIterable(queue)
            .flatMapSingle { clickedSteps ->
                clickedSteps.request
                    .map { GameScoreStepsUpdate(clickedSteps.gameSide, it) }
                    .flatMap { gameApi.sendStepsScores(it, gameId) }
                    .map { clickedSteps.index }
            }
            .toList()
            .map { sentIndexes ->
                queue
                    .toMutableList()
                    .apply { removeAll { sentIndexes.contains(it.index) } }
            }
    }


    fun onClick(gameApi: GameAPI?, clickSide: Int) {
        if (!isSideSelected) {
            onSelectSide(clickSide)
            return
        }
        applyClickCurrentStep(stepIndex, clickSide)
        if (clickedSteps.size >= AMOUNT_CLICKED_STEPS_TO_SEND) {
            prepareSendStepsQueue()
            gameApi?.let { sendSteps(gameApi) }
        }
        startNextStepTimer(500)
    }

    private fun applyClickCurrentStep(stepIndex: Int, clickSide: Int) {
        if (stepIndex < steps.size) {
            val step = steps[stepIndex]
            val point = GamePoint(step.idStep, step.point[clickSide])
            getClickedPoint.value = StepGamePoint(
                value = if (point.point > 0) {
                    "+${point.point}"
                } else {
                    "${point.point}"
                },
                isPositive = point.point > 0
            )
            clickedSteps.add(point)
        }
    }

    private fun applyNextStep(nextStepIndex: Int) {
        if (nextStepIndex < steps.size) {
            val nextStep = steps[nextStepIndex]
            isLeftButtonActivated.value = nextStep.point[0] > 0
            isRightButtonActivated.value = nextStep.point[0] < 0
            startSkipTimerOnNoAction(nextStep.duration)
        }
        if (nextStepIndex >= steps.size) {
            waiting.value = Unit
        }
    }

    private fun prepareSendStepsQueue() {
        val request = SendStepRequest(
            index = clickedStepsSendQueue.size,
            gameSide = gameSide,
            request = Single.just(clickedSteps.toList())
        )
        clickedStepsSendQueue.add(request)
        clickedSteps.clear()
    }

    private fun sendSteps(gameApi: GameAPI, onFinished: () -> Unit = {}) {
        val toSend = clickedStepsSendQueue.toList()
        clickedStepsSendQueue.clear()
        val disposable = sendStepsFromQueue(gameApi, id, toSend)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe(
                {
                    clickedStepsSendQueue.addAll(0, it)
                    onFinished()
                },
                { onFinished() }
            )
        compositeDisposable.add(disposable)
    }

    fun onDestroy() {
        compositeDisposable.clear()
    }

    private fun onSuccessGame(api: GameAPI, response: GameStepsResponse) {
        title = response.game.title
        gameEnd = response.game.endTime
        lastDuration = (response.game.endTime - System.currentTimeMillis() / 1000L).toInt()
        titleData.value = title
        time.value = lastDuration
        steps = response.steps
        isGameStarted.value = false
        val oneSecondInterval = timerUpdateInterval
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { onSuccessSetTime(api, it) },
                this::onErrorSetTime
            )
        compositeDisposable.add(oneSecondInterval)
    }

    private fun onErrorTime(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    private fun onSuccessSetTime(gameApi: GameAPI, value: Long) {
        lastDuration = (gameEnd - System.currentTimeMillis() / 1000L).toInt()
        if (lastDuration > -1) {
            time.value = lastDuration
        }
        if (lastDuration == 0) {
            finishGame(gameApi)
        }
    }

    private fun finishGame(gameApi: GameAPI) {
        waiting.value = Unit
        timerSkip?.let { compositeDisposable.remove(it) }
        timerSkip?.dispose()
        timerToStartNextStep?.let { compositeDisposable.remove(it) }
        timerToStartNextStep?.dispose()
        setStepDisabled()
        if (clickedStepsSendQueue.isNotEmpty()) {
            prepareSendStepsQueue()
            sendSteps(gameApi) {
                getFinishScores(gameApi)
            }
        } else {
            getFinishScores(gameApi)
        }
    }

    private fun getFinishScores(gameApi: GameAPI) {
        val getGameRequest = gameApi.getStepsGame(id)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { closeWithResult.value = it.game.scores },
                { closeWithResult.value = Collections.emptyList() }
            )
        compositeDisposable.add(getGameRequest)
    }

    private fun onErrorSetTime(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

    private fun startSkipTimerOnNoAction(time: Int) {
        timerSkip?.let { compositeDisposable.remove(it) }
        timerSkip?.dispose()
        timerSkip = Observable.timer(time.toLong(), TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .subscribe(
                {
                    startNextStepTimer(500)
                },
                this::onErrorSetTime
            )
        timerSkip?.let { compositeDisposable.add(it) }
    }

    private fun startNextStepTimer(time: Int) {
        setStepDisabled()
        timerSkip?.let { compositeDisposable.remove(it) }
        timerSkip?.dispose()
        timerToStartNextStep?.let { compositeDisposable.remove(it) }
        timerToStartNextStep?.dispose()
        timerToStartNextStep = Observable.timer(time.toLong(), TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.computation())
            .subscribe(
                {
                    isClicksEnabled.value = true
                    stepIndex++
                    applyNextStep(stepIndex)
                },
                this::onErrorSetTime
            )
        timerToStartNextStep?.let { compositeDisposable.add(it) }
    }

    private fun setStepDisabled() {
        isClicksEnabled.value = false
        isLeftButtonActivated.value = false
        isRightButtonActivated.value = false
    }

    private class SendStepRequest(
        val index: Int,
        val gameSide: Int,
        val request: Single<List<GamePoint>>,
    )
}
