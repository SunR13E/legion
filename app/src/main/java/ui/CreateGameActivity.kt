package ui

import android.content.Intent
import android.os.Bundle
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.core.widget.addTextChangedListener
import androidx.lifecycle.ViewModelProviders
import com.example.legion.R
import com.google.android.material.switchmaterial.SwitchMaterial
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import data.GameAPI
import ui.step.StepsGameActivity

class CreateGameActivity : AppCompatActivity() {

    private lateinit var gameTypeDescriptionText: TextView
    private lateinit var gameTypeDescriptionIcon: ImageView

    private val gameApi: GameAPI by lazy { (application as GameApp).gameApi }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_game)

        val viewModel = ViewModelProviders.of(this).get(CreateGameViewModel::class.java)

        val switcher = findViewById<SwitchMaterial>(R.id.switcher)
        switcher.setOnCheckedChangeListener { _, isChecked -> viewModel.onSelectType(isChecked) }
        gameTypeDescriptionText = findViewById(R.id.game_type_description)
        gameTypeDescriptionIcon = findViewById(R.id.icon_game_type)
        val setGame = findViewById<TextView>(R.id.set_game)
        val titleText = findViewById<TextInputEditText>(R.id.title)
        val titleContainer = findViewById<TextInputLayout>(R.id.title_container)
        val durationText = findViewById<TextInputEditText>(R.id.duration)
        val durationContainer = findViewById<TextInputLayout>(R.id.duration_container)
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.setTitle(R.string.create_game)

        titleText.addTextChangedListener { titleContainer.error = null }
        titleText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                titleText.setSelection(titleText.text?.length ?: 0)
            }
        }
        durationText.addTextChangedListener { durationContainer.error = null }
        durationText.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                durationText.setSelection(durationText.text?.length ?: 0)
            }
        }

        viewModel.isStepGameType.observe(this, {
            switcher.isChecked = it
            setGameType(it)
        })
        viewModel.isErrorTitle.observe(this, { isEnabled ->
            if (isEnabled) {
                titleContainer.error = getString(R.string.input_game_title_error)
            } else {
                titleContainer.error = null
            }
        })
        viewModel.isErrorDurationMax.observe(this, { (isEnabled, duration) ->
            if (isEnabled) {
                durationContainer.error =
                    getString(R.string.input_game_time_in_seconds_error_time_max, duration)
            } else {
                durationContainer.error = null
            }
        })
        viewModel.isErrorDurationMin.observe(this, { (isEnabled, duration) ->
            if (isEnabled) {
                durationContainer.error =
                    getString(R.string.input_game_time_in_seconds_error_time_min, duration)
            } else {
                durationContainer.error = null
            }
        })
        viewModel.startGame.observe(this, {
            when (it) {
                is GameNavigation.Vote -> startVote(it.gameId)
                is GameNavigation.Step -> startSteps(it.gameId)
            }
        })
        viewModel.durationRecommended.observe(this, {
            durationText.setText(it.toString())
        })

        setGame.setOnClickListener {
            viewModel.setGame(
                gameApi = gameApi,
                title = titleText.text.toString(),
                duration = durationText.text.toString()
            )
        }

        titleContainer.requestFocus()
        viewModel.onCreate(gameApi)
    }

    private fun startSteps(id: String) {
        val intent = Intent(this, StepsGameActivity::class.java)
        intent.putExtra("SETTINGS", id)
        startActivity(intent)
        finish()
    }

    private fun startVote(id: String) {
        val intent = Intent(this, VoteGameActivity::class.java)
        intent.putExtra("SETTINGS", id)
        startActivity(intent)
        finish()
    }

    private fun setGameType(isChecked: Boolean) {
        val color = ContextCompat.getColor(this, R.color.black)
        if (isChecked) {
            val drawable = ContextCompat.getDrawable(this, R.drawable.ic_steps)?.apply {
                setTint(color)
            }
            gameTypeDescriptionIcon.setImageDrawable(drawable)
            gameTypeDescriptionText.setText(R.string.game_type_step_description)
        } else {
            val drawable = ContextCompat.getDrawable(this, R.drawable.ic_vote)?.apply {
                setTint(color)
            }
            gameTypeDescriptionIcon.setImageDrawable(drawable)
            gameTypeDescriptionText.setText(R.string.game_type_vote_description)
        }
    }
}
