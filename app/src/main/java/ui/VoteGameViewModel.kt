package ui

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import data.GameAPI
import data.GameVoteResponse
import data.ScoreResponse
import data.SendScoreRequest
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class VoteGameViewModel() : ViewModel() {

    private var amount1 : Int = 0
    private var amount2 : Int = 0
    private val compositeDisposable = CompositeDisposable()
    var id : String = ""
    val time : MutableLiveData<Int> = MutableLiveData()
    val close : MutableLiveData<List<Int>> = MutableLiveData()
    val titleData : MutableLiveData<String> = MutableLiveData()
    val data: MutableLiveData<List<Int>> = MutableLiveData()
    val allgame: MutableLiveData<GameVoteResponse> = MutableLiveData()
    val updateScores: MutableLiveData<Unit> = MutableLiveData()
    var lastDuration : Int = 0
    var title : String = ""

    private val interval = Observable.interval(1000L, TimeUnit.MILLISECONDS)

    fun increase1(): Int {
        amount1++
        return amount1
    }

    fun increase2(): Int{
        amount2++
        return amount2
    }

    fun onActivityCreated(gameApi : GameAPI?){
        val oneSecondInterval = interval
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                this::onSuccessSetTime,
                this::onErrorSetTime
            )
        compositeDisposable.add(oneSecondInterval)

        gameApi?.let{ api ->
            val getGameRequest = api.getVoteGame(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onSuccessGame,
                    this::onErrorTime
                )
            compositeDisposable.add(getGameRequest)
        }
    }

    fun game(gameApi : GameAPI?){
        gameApi?.let{ api ->
            val scores = api.sendVoteScores(SendScoreRequest(amountClicks = listOf(amount1, amount2)), id = id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onSuccessGameScore,
                    this::onErrorGame
                )

            compositeDisposable.add(scores)
            amount2 = 0
            amount1 = 0
        }
    }

    fun ActivityOnDestroy(){
        compositeDisposable.clear()
    }

    private fun onSuccessGameScore(score: ScoreResponse) {
        if(score.isFinished){
            close.value = score.scores
        }
        data.value = score.scores
    }

    private fun onErrorGame(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    private fun onSuccessGame(response : GameVoteResponse) {
        title = response.game.title
        lastDuration = (response.game.endTime - System.currentTimeMillis()/1000L).toInt()
        titleData.value = title
        time.value = lastDuration
        allgame.value = response
    }

    private fun onErrorTime(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    private fun onSuccessSetTime(value : Long) {
        lastDuration -= 1
        if(lastDuration > -1) {
            time.value = lastDuration
            updateScores.value = Unit
        }
    }

    private fun onErrorSetTime(throwable: Throwable) {
        Log.e("TAG", throwable.message.toString())
    }

    override fun onCleared() {
        compositeDisposable.clear()
        super.onCleared()
    }

}