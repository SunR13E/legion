package data

import com.google.gson.annotations.SerializedName

data class GameListResponse(
    @SerializedName("games")
    val games: List<GameShort>?,
    @SerializedName("count")
    val count: Int,
    @SerializedName("errors")
    val errors: List<String>?,
)

data class GameShort(
    @SerializedName("id")
    val id: String,
    @SerializedName("title")
    val title: String,
    @SerializedName("end_time")
    val endTime: Long,
    @SerializedName("score")
    val score: List<Int>,
    val type: Int,
)

data class GameIdResponse(
    @SerializedName("id")
    val id: String,
    @SerializedName("errors")
    val errors: List<String>,
)

data class GameVoteResponse(
    val game: GameVote,
    val errors: List<String>,
)

data class GameVote(
    val id: String,
    val title: String,
    @SerializedName("end_time")
    val endTime: Long,
    val scores: List<Int>,
)

data class GameStepsResponse(
    val game: GameSteps,
    val steps: List<Steps>,
    val errors: List<String>,
)

data class GameSteps(
    val id: String,
    val title: String,
    @SerializedName("end_time")
    val endTime: Long,
    @SerializedName("score")
    val scores: List<Int>,
)

data class Steps(
    @SerializedName("id")
    val idStep: String,
    val duration: Int,
    @SerializedName("points")
    val point: List<Int>,
)

data class Score(
    val scores: List<Int>,
    val errors: List<String>,
)

data class ScoreResponse(
    @SerializedName("is_finished")
    val isFinished: Boolean,
    val scores: List<Int>,
    val errors: List<String>,
)

data class CreateGameRequest(
    val duration: Int,
    val title: String,
    val type: Int,
)

data class SendScoreRequest(
    @SerializedName("amount_clicks")
    val amountClicks: List<Int>,
)

data class GameScoreStepsUpdate(
    @SerializedName("game_side")
    val gameSide: Int,
    val points: List<GamePoint>,
)

data class GamePoint(
    val stepId: String,
    val point: Int,
)


//curl -X GET "http://81.163.23.101:9098/games?limit=100&offset=0"
//curl -X GET "http://81.163.23.101:9098/games/settings"
//
//curl -X GET "http://81.163.23.101:9098/games/{cb39a23e-e52f-45ea-918e-c2b14ca66ce9}"
//
//curl -X GET "http://192.168.1.3:9091/games/{5}/scores"
//
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"1", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"2", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"3", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"4", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"5", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"6", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"7", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"8", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"9", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"10", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"11", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"12", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"13", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"14", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"15", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"16", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"17", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"18", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"19", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"20", "duration":999, "type":0 }'
//curl -X POST "http://81.163.23.101:9098/games/add" -d '{ "title":"21", "duration":999, "type":0 }'