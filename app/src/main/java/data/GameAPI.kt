package data

import io.reactivex.Single
import retrofit2.http.*

interface GameAPI {

    @GET("/games/{id}")
    @Headers("Content-Type: application/json")
    fun getVoteGame(@Path("id")id: String) : Single<GameVoteResponse>

    @GET("/games/{id}")
    @Headers("Content-Type: application/json")
    fun getStepsGame(@Path("id")id: String) : Single<GameStepsResponse>

    @GET("/games")
    @Headers("Content-Type: application/json")
    fun getGames(@Query("limit") limit: Int, @Query("offset") offset: Int) : Single<GameListResponse>

    @GET("/games/{id}/scores")
    @Headers("Content-Type: application/json")
    fun getScore(@Path("id")id : String) : Single<Score>

    @POST("/games/{id}/scores/update")
    @Headers("Content-Type: application/json")
    fun sendVoteScores(@Body()scores: SendScoreRequest, @Path("id")id : String) : Single<ScoreResponse>

    @POST("/games/add")
    @Headers("Content-Type: application/json")
    fun createGame(@Body()gameSettings : CreateGameRequest) : Single<GameIdResponse>

    @GET("/games/settings")
    @Headers("Content-Type: application/json")
    fun getGameSettings(): Single<GameSettings>

    @POST("/games/{id}/scores/steps/update")
    @Headers("Content-Type: application/json")
    fun sendStepsScores(@Body()scores: GameScoreStepsUpdate, @Path("id")id : String) : Single<ScoreResponse>

}
